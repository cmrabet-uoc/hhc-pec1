/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para la gestión de llamadas a la API de MARVEL.
 * api.js
 */

// Esto es para Parcel, para resolver el problema explicado aqui:
// https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
import 'regenerator-runtime/runtime';

// Este es un Polyfill de fetch para navegadores antiguos.
import 'whatwg-fetch';

import ui from '../lib/ui';

// URL de la API de MARVEL.
const API_URL = 'https://gateway.marvel.com/v1/';

// Clave de autorización de consultas a la API de MARVEL.
//const API_KEY = '37fa8079557563b145906852f25d7065';
const API_KEY = 'd0729b11fb6b144f59fae91b8f0e32ee';

// Exportamos todas las funciones para se puedan llamar desde otros scripts.
export default {
  getSeriesByYear,
  getSeriesById,
  getLastComics,
  getComicsBySeries,
  getComicById,
  getCreators,
  getCharacters,
  getCharacterById,
  getCreatorById,
};

/**
 * Hace una consulta a un endpoint con las opciones pasadas
 * como argumento.
 * Esto es un wrapper que encapsula el uso de fetch.
 */

async function api(endpoint, options = {}) {
  const url = new URL(`public/${endpoint}`, API_URL);

  options.apikey = API_KEY;
  url.search = new URLSearchParams(options);

  try {
    // Hacemos la consulta a la API.
    const res = await fetch(url.toString(), {
      headers: {
        'Accept-Encoding': 'gzip',
      },
    });

    // Si hay un error en la respuesta, mostramos un diálogo con el
    // error devuelto.
    // Para algunos errores con concreto, customizamos el mensaje.
    if (!res.ok) {
      let msg = res.statusText;

      switch (res.status) {
        case 401:
          msg =
            'The connection to the <cite class="tm">MARVEL</cite> API was not authorized.';
          break;
        case 404:
          msg = 'The content you are trying to access does not exist.';
          break;
        case 429:
          msg =
            'You have reached the maximum amount of allowed requests to the <cite class="tm">MARVEL</cite> API.';
          break;
      }

      // Cuando el usuario acepta el diálogo, le mandamos a la página
      // de la que vino.
      ui.showError('Oops!!', msg, () => location.reload());
      return;
    }

    // Todo ok, devolvemos el payload en la respuesta en formato JSON.
    const data = await res.json();

    return data;
  } catch (e) {
    ui.showError(
      'Oops!!',
      'The <cite class="tm">MARVEL</cite> datacenter is not reachable. Please, check your Internet connection, and when ready, press the button to try again.',
      () => location.reload()
    );
  }
}

/**
 * Esta función es una implementación muy rudimentaria de una cache
 * que he hecho para ahorrar consultas a la API de MARVEL, las cuales
 * están limitadas a 3000 al día. Para ello uso localStorage, sin embargo
 * antes de lanzar la web en producción habría que implementar algo mucho
 * más robusto que optimice los datos guardados en localStorage.
 *
 * Busca en localStorage un entrada con una clave compuesta por el endpoint
 * y opciones pasadas como argumento, y la devuelve para su consumo sin hacer
 * una llamada a la API de MARVEL. Si la entrada no se encuentra, hace la
 * llamada a la API de MARVEL y guarda el resultado con dicha clave.
 */

async function getFromCache(endpoint, options) {
  const key = JSON.stringify({ endpoint, options });
  const storedData = localStorage.getItem(key);

  if (storedData) {
    return JSON.parse(storedData);
  }

  const newData = await api(endpoint, options);
  localStorage.setItem(key, JSON.stringify(newData));

  return newData;
}

/**
 * Devuelve las series de comics publicadas en el año pasado como argumento.
 */

async function getSeriesByYear(startYear, limit, offset = 0) {
  const fetchData = await getFromCache('series', {
    startYear,
    limit,
    offset,
  });

  if (fetchData.data && fetchData.data.results) {
    const { offset, total, results } = fetchData.data;

    return {
      offset,
      total,
      results,
    };
  }

  return [];
}

/**
 * Devuelve la serie con id pasado como argumento.
 */

async function getSeriesById(seriesId) {
  const fetchData = await getFromCache(`series/${seriesId}`);
  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results[0];
  }

  return null;
}

/**
 * Devuelve los últimos 4 comics publicados.
 */

async function getLastComics() {
  const fetchData = await getFromCache('comics', {
    orderBy: '-focDate',
    limit: 4,
    noVariants: true,
  });

  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results;
  }

  return [];
}

/**
 * Devuelve los comics publicados en la serie con el id pasado como argumento.
 */

async function getComicsBySeries(seriesId) {
  const fetchData = await getFromCache(`series/${seriesId}/comics`, {
    orderBy: 'issueNumber',
  });

  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results;
  }

  return [];
}

/**
 * Devuelve el comic con el id pasado como argumento.
 */

async function getComicById(id) {
  const fetchData = await getFromCache(`comics/${id}`);

  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results[0];
  }

  return null;
}

/**
 * Devuelve los autores cuyo nombre empieza con la letra pasada como argumento.
 */

async function getCreators(letter, limit, page = 0) {
  const fetchData = await getFromCache(`creators`, {
    nameStartsWith: letter,
    orderBy: 'lastName',
    limit,
    offset: page,
  });

  if (fetchData.data && fetchData.data.results) {
    const { offset, total, results } = fetchData.data;

    return {
      offset,
      total,
      results,
    };
  }

  return [];
}

/**
 * Devuelve los personajes cuyo nombre empieza con la letra pasada como argumento.
 */

async function getCharacters(letter, limit, page = 0) {
  const fetchData = await getFromCache(`characters`, {
    nameStartsWith: letter,
    limit,
    offset: page,
  });

  if (fetchData.data && fetchData.data.results) {
    const { offset, total, results } = fetchData.data;

    return {
      offset,
      total,
      results,
    };
  }

  return [];
}

/**
 * Devuelve el personaje con el id pasado como argumento.
 */

async function getCharacterById(id) {
  const fetchData = await getFromCache(`characters/${id}`);

  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results[0];
  }

  return null;
}

/**
 * Devuelve al autor con el id ppasado como argumento.
 */

async function getCreatorById(id) {
  const fetchData = await getFromCache(`creators/${id}`);

  if (fetchData.data && fetchData.data.results) {
    return fetchData.data.results[0];
  }

  return null;
}
