/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para la gestión de operaciones comunes con elementos DOM.
 * ui.js
 */

// Exportamos funciones para que puedan ser usadas desde otros scripts.

const ui = {
  showError,
  removeDates,
  formatYears,
  formatTimestamp,
  a,
  p,
  li,
  img,
  button,
  dl,
  card,
  record,
  paginator,
  createComicDetails,
  getIdFromURI,
  toHTTPS,
};

export default ui;

/**
 * Muestra el diálogo de error con el título y mensaje pasados como argumento.
 * Además, si se pasa una función cb como argumento (callback) será ejecutada
 * cuando el usuario presione el botón de aceptar.
 */

function showError(title, text, cb) {
  const errorBoxEl = document.querySelector('.js-error');
  const titleEl = errorBoxEl.querySelector('.js-error-title');
  const textEl = errorBoxEl.querySelector('.js-error-text');
  const acceptEl = errorBoxEl.querySelector('.js-error-accept');

  titleEl.innerText = title;
  textEl.innerHTML = text;
  errorBoxEl.classList.add('alert--is-visible');

  acceptEl.addEventListener('click', function () {
    errorBoxEl.classList.remove('alert--is-visible');
    cb && cb();
  });
}

/**
 * Elimina las fechas que la API MARVEL pone al final de cada título de serie
 * o cómic.
 */

function removeDates(text) {
  return text.trim().slice(0, text.indexOf('(') - 1);
}

/**
 * Devuelve una cadena de texto con un año o rango de años en base a los años
 * inicial y final pasados.
 */

function formatYears(startYear, endYear) {
  return startYear !== endYear
    ? `<time datetime="${startYear}">${startYear}</time> - <time datetime="${endYear}">${endYear}</time>`
    : `<time datetime="${startYear}">${startYear}</time>`;
}

/**
 * Formatea un valor timestamp a fecha en formato humano.
 */

function formatTimestamp(timestamp) {
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const date = new Date(timestamp);
  const fmtDate = `${
    months[date.getMonth()]
  } ${date.getDate()}, ${date.getFullYear()}`;

  return `<time datetime="${timestamp}">${fmtDate}</time>`;
}

/**
 * Crea y devuelve un elemento DOM <a> con el url y clase pasados como argumento.
 */

function a(url, className) {
  const a = document.createElement('a');
  a.href = url;
  className && a.classList.add(className);
  return a;
}

/**
 * Crea y devuelve un elemento DOM <p> con la clase pasada como argumento.
 */

function p(className) {
  const p = document.createElement('p');
  className && p.classList.add(className);
  return p;
}

/**
 * Crea y devuelve un elemento DOM <li> con la clase pasada como argumento.
 */

function li(className) {
  const li = document.createElement('li');
  className && li.classList.add(className);
  return li;
}

/**
 * Crea y devuelve un elemento DOM <img> con el path a la imágen pasada como argumento.
 * Se puede además especificar una descripción para la imágen (title) y clase.
 */

function img(image, description = '', className) {
  const img = document.createElement('img');
  img.classList.add(className);
  img.src = image;
  img.alt = `${description}`;
  return img;
}

/**
 * Crea y devuelve un elemento DOM <button> con el texto y clase pasados como argumento.
 */

function button(text, className) {
  const btn = document.createElement('button');
  btn.type = 'button';
  btn.innerText = text;
  className && btn.classList.add(className);
  return btn;
}

/**
 * Crea un elemento DOM <dl> dentro del elemento padre DOM pasado como argumento.
 * El contenido de la lista de descripción se pasa como argumento en un objeto clave: valor.
 * Por defecto solo se añadiran elementos <dt> y <dd> que tengan tanto clave como valor, si
 * se quiere enseñar elementos <dt> sin valores, se puede pasar el argumento showEmpty como true.
 * Para alinear a la izquierda el contenido de la lista, se puede pasar el argumento isLeft como true.
 */

function dl(values, showEmpty, isLeft, parentEl) {
  const dl = document.createElement('dl');
  dl.classList.add('details');

  isLeft && dl.classList.add('details--is-left');

  Object.keys(values).forEach((key) => {
    const dt = document.createElement('dt');
    const dd = document.createElement('dd');

    if (!showEmpty && !values[key]) {
      return;
    }

    dt.classList.add('details__term');
    dt.innerText = key;
    dd.classList.add('details__description');
    dd.innerHTML = values[key];

    if (parentEl) {
      parentEl.appendChild(dt);
      parentEl.appendChild(dd);
      return;
    }

    dl.appendChild(dt);
    dl.appendChild(dd);
  });

  return dl;
}

/**
 * Crea y devuelve un componente card, con el título, imágen, destino y clases pasados como argumento.
 * Este componente está construido con un elemento DOM <figure>.
 * El destino es la URL a dónde se enlazará el usuario cuando haga click sobre el componente.
 */

function card({ title, image, url, classes }) {
  const a = ui.a(url, 'card');
  a.title = `Go to ${title}`;
  classes && a.classList.add(classes);

  const figure = document.createElement('figure');

  const figcaption = document.createElement('figcaption');
  figcaption.classList.add('card__description');
  figcaption.innerHTML = `<cite>${title}</cite>`;

  const img = ui.img(image, title, 'card__image');

  figure.appendChild(img);
  figure.appendChild(figcaption);

  a.appendChild(figure);

  return a;
}

/**
 * Crea y devuelve un componente record, con el título, imágen, destino y detalles pasados como argumento.
 * Este componente está construido con un elemento DOM <figure>.
 * El destino es la URL a dónde se enlazará el usuario cuando haga click sobre el record.
 * Los detalles son un objeto clave:valor utilizados para construír una lista de descripción.
 */

function record({ title, image, url, details }) {
  const a = ui.a(url, 'record');
  a.title = `Go to series "${title}"`;

  const p = ui.p('record__title');
  p.innerHTML = `<cite>${title}</cite>`;
  p.classList.add('t-tag');
  a.appendChild(p);

  const figure = document.createElement('figure');
  figure.classList.add('record__body');

  const figcaption = document.createElement('figcaption');
  figcaption.classList.add('record__details');

  const dl = ui.dl(details);
  figcaption.appendChild(dl);

  const img = ui.img(image, title, 'record__image');

  figure.appendChild(img);
  figure.appendChild(figcaption);

  a.appendChild(figure);

  return a;
}

/**
 * Gestiona el comportamiento del componente paginador.
 */

function paginator(itemsPage, offset, total, cb) {
  const pagePrevEl = document.querySelector('.js-page-prev');
  const pageNextEl = document.querySelector('.js-page-next');
  const pageButtonsEl = document.querySelector('.js-page-buttons');

  // Calculamos los valores de página en base a número total
  // de elementos devueltos por la consulta a la API, y el
  // máximo permitido por página.
  const totalPages = Math.ceil(total / itemsPage);
  const currentPage = Math.ceil(offset / itemsPage);

  // Populamos el interior del paginador con tantos enlaces
  // como páginas disponibles.
  pageButtonsEl.innerHTML = '';

  // Creamos los enlaces interiores para cada página.
  [...Array(totalPages)].forEach((_, page) => {
    const pageButton = ui.button('', 'paginator__item');

    if (page === currentPage) {
      pageButton.classList.add('paginator__item--is-selected');
    } else {
      pageButton.title = `Load page number ${page + 1}`;
    }

    pageButton.innerText = page + 1;
    pageButtonsEl.appendChild(pageButton);

    pageButton.addEventListener('click', (e) => {
      e.preventDefault();

      cb(page * itemsPage);
    });
  });

  // Actualizamos el estado de los enlaces de página anterior
  // y posterior.

  if (currentPage < 1) {
    pagePrevEl.classList.add('paginator__item--is-disabled');
  } else {
    pagePrevEl.classList.remove('paginator__item--is-disabled');
  }

  pagePrevEl.addEventListener('click', (e) => {
    e.preventDefault();

    const newOffset = currentPage ? (currentPage - 1) * itemsPage : 0;
    cb(newOffset);
  });

  if (currentPage == totalPages - 1) {
    pageNextEl.classList.add('paginator__item--is-disabled');
  } else {
    pageNextEl.classList.remove('paginator__item--is-disabled');
  }

  pageNextEl.addEventListener('click', (e) => {
    e.preventDefault();

    const newOffset = (currentPage + 1) * itemsPage;
    cb(newOffset);
  });
}

/**
 * Crea y devuelve el componente que resume los detalles de un cómic.
 * Este componente está construido con un elemento DOM <figure>.
 * Los detalles son un objeto clave:valor utilizados para construír una lista de descripción.
 */

function createComicDetails({ details }) {
  const figure = document.createElement('figure');
  figure.classList.add('file__body');

  const figcaption = document.createElement('figcaption');
  figcaption.classList.add('file__details');

  const dl = ui.dl(details, true, true);
  figcaption.appendChild(dl);

  figure.appendChild(figcaption);

  return figure;
}

/**
 * Extrae y devuelve el id de la URL pasada como argumento.
 */

function getIdFromURI(uri) {
  return uri.split('/').pop();
}

/**
 * Reemplaza http por https en una URL pasada como argumento.
 */

function toHTTPS(url) {
  return url.replace(/^http:\/\//i, 'https://');
}
