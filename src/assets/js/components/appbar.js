/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para la gestión del componente AppBar.
 * appbar.js
 */

export default { setCurrentPage };

/**
 * Marca como seleccionado la opción del menú cuyo id es pasado como argumento.
 */

function setCurrentPage(selectedId) {
  const appbarMenuItems = document.querySelectorAll('.js-appbar-menu li');

  appbarMenuItems.forEach((item) => {
    const a = item.querySelector('a');
    if (a) {
      if (item.id === selectedId) {
        a.classList.add('appbar__menu-item--state-selected');
        return;
      }
      item.classList.remove('appbar__menu-item--state-selected');
    }
  });
}
