/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página creator.html.
 * creator.js
 * Muestra información sobre el autor cuyo id se pasa
 * en la query de la URL de la página.
 */

import api from '../lib/api';
import appbar from '../components/appbar';
import ui from '../lib/ui';

// Elemento DOM que contiene la foto del autor.
const creatorNameEl = document.querySelector('.js-creator-name');

// Elemento DOM <ul> que contiene la lista de los comics en los que
// ha trabajado el autor.
const comicsEl = document.querySelector('.js-creator-comics');

// Elemento DOM <ul> que contiene la lista de series en las que
// ha trabajado el autor.
const seriesEl = document.querySelector('.js-creator-series');

// Array de elementos DOM que mostrarán el nombre del autor.
const authorNamesEls = document.querySelectorAll('.js-creator-name');

// Elemento DOM que mostrará el número total de series en las que ha
// trabajado el autor.
const seriesCountEl = document.querySelector('.js-creator-series-count');

// Elemento DOM que mostrará el número total de comics en las que ha
// trabajado el autor.
const comicsCountEl = document.querySelector('.js-creator-comics-count');

// Seleccionamos en el menú principal el enlace para autores.
appbar.setCurrentPage('creators');

// Populamos el contenido de la página.
updateContent();

async function updateContent() {
  // Extraemos de la URL el id del personaje.
  const urlParams = new URLSearchParams(window.location.search);
  const creatorId = urlParams.get('id');

  if (creatorId) {
    // Si el id es correcto, obtenemos de la API los datos del autor.
    const creator = await api.getCreatorById(creatorId);

    if (creator) {
      // Si el autor existe, poulamos su foto y detalles con los
      // datos devueltos por la API.
      const { fullName, comics, series } = creator;

      creatorNameEl.innerText = fullName;
      authorNamesEls.forEach((item) => (item.innerHTML = fullName));

      seriesCountEl.innerText = series.items.length;
      comicsCountEl.innerText = comics.items.length;

      // Populamos los comics y series en las que ha trabajado el autor.
      createComicsList(comics.items);
      createSeriesList(series.items);
    }
  }
}

// Popula la lista de comics en las que ha trabajado el autor.
async function createComicsList(items) {
  comicsEl.innerHTML = '';

  items.forEach(async (item) => {
    // Por cada comic, obtenemos su informacion y creamos un component card usando
    // utilidades del script ui.js.
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getComicById(id);

    const url = `comic.html?id=${id}`;
    const image = `${data.thumbnail.path}/portrait_fantastic.${data.thumbnail.extension}`;
    const card = ui.card({
      title: data.title,
      image,
      url,
    });
    card.classList.add('card--is-comic');

    const comic = ui.li();
    comic.classList.add('catalog__item');
    comic.appendChild(card);

    comicsEl.appendChild(comic);
  });

  if (items.length) {
    comicsEl.classList.add('l-catalog--is-full');
  }
}

// Popula la lista de series en las que ha trabajado el personaje.
async function createSeriesList(items) {
  seriesEl.innerHTML = '';

  items.forEach(async (item) => {
    // Por cada serie, obtenemos su informacion y creamos un elemento <li>
    // con un enlace a la serie.
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getSeriesById(id);

    const url = `series-detail.html?id=${id}`;

    const li = ui.li();
    li.classList.add('l-center');

    const a = ui.a(url, 'link');
    a.classList.add('t-text');
    a.innerText = data.title;
    li.appendChild(a);

    seriesEl.appendChild(li);
  });
}
