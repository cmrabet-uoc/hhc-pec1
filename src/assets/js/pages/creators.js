/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página creators.html.
 * creators.js
 * Muestra una lista de autores cuyos nombres empiezan
 * con la letra seleccionada por el usuario.
 */

import api from '../lib/api';
import ui from '../lib/ui';
import appbar from '../components/appbar';

// Número máximo de resultados por página
const ITEMS_PAGE = 50;

// Almacena el nombre de la clave de localStorage para guardar el último
// personaje seleccionado.
// Lo declaro como constante para que sea más cómodo alterar en un futuro.
const LETTER_STORE_NAME = 'marvel-creators-letter';

// Elemento DOM <select> que contendrá las letras del abecedario para
// filtrado de autores.
const abcListEl = document.querySelector('.js-creators-abc');

// Elemento DOM <ul> que contendrá los autores filtrados.
const caractersListEl = document.querySelector('.js-creators-list');

// Seleccionamos en el menú principal el enlace para autores.
appbar.setCurrentPage('creators');

// Llenamos la lista con letras del abecedario.
populateAbc(lastUsedLetter());

// Populamos el contenido de la página.
updateContent(lastUsedLetter());

// Añadimos un listener al evento change y lo respondemos cuando se seleccione
// una nueva letra de abecedario.
abcListEl.addEventListener('change', async function (e) {
  e.preventDefault();
  const selectedLetter = e.target.value || 'A';
  await updateContent(selectedLetter);
  localStorage.setItem(LETTER_STORE_NAME, selectedLetter);
});

// Devuelve la última letra usada (la lee del localStorage).
function lastUsedLetter() {
  return localStorage.getItem(LETTER_STORE_NAME) || 'A';
}

// Llena la lista de letras.
function populateAbc(selectedLetter) {
  [...Array(26)].forEach((_, code) => {
    const letter = String.fromCharCode(code + 65);

    const newOption = document.createElement('option');
    newOption.value = letter;
    newOption.innerHTML = letter;
    abcListEl.appendChild(newOption);
  });

  abcListEl.value = selectedLetter;
}

async function updateContent(letter, offset = 0) {
  
  // Obtenemos de la API los autores cuyos nombres empiezan
  // por la letra seleccionada.
  const creators = await api.getCreators(letter, ITEMS_PAGE, offset);
  
  caractersListEl.innerHTML = '';
  
  // Delegamos al paginador las llamadas consecuentes a la API.
  ui.paginator(ITEMS_PAGE, creators.offset, creators.total, (offset) => {
    updateContent(lastUsedLetter(), offset);
  });

  creators.results.forEach(async (item) => {
    const url = `creator.html?id=${item.id}`;

    const creatorEl = ui.li();
    const a = ui.a(url, 'link');
    a.classList.add('t-text');
    a.innerHTML = `<cite>${item.fullName}</cite>`;
    creatorEl.appendChild(a);

    caractersListEl.appendChild(creatorEl);
  });
}
