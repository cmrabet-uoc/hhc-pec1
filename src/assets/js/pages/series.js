/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página series.html.
 * series.js
 * Muestra una lista con las series publicadas en el año
 * seleccionado por el usuario.
 */

import api from '../lib/api';
import ui from '../lib/ui';
import appbar from '../components/appbar';

// Número máximo de resultados por página
const ITEMS_PAGE = 10;

// Almacena el nombre de la clave de localStorage para guardar el año seleccionado.
// Lo declaro como constante para que sea más cómodo alterar en un futuro.
const YEAR_STORE_NAME = 'marvel-series-year';

// Almacena el año actual.
const currentYear = new Date().getFullYear();

// Elemento DOM <select> que contiene los años seleccionables.
const seriesYearSelect = document.querySelector('.js-series-year');

// Elemento DOM <ul> que contiene todas las series en el año seleccionado.
const seriesCatalog = document.querySelector('.js-series-catalog');

// Seleccionamos en el menú principal el enlace para series.
appbar.setCurrentPage('series');

// Llenamos el elemento DOM select con todos los años disponibles.
populateList(lastUsedYear());

// Llenamos el elemento DOM ul con elementos <li> contiendo el markup
// para cada serie.
updateSeries(lastUsedYear());

// Añadimos un listener al evento change y lo respondemos cuando se seleccione
// un nuevo año.
seriesYearSelect.addEventListener('change', function (e) {
  const year = +e.target.value;
  updateSeries(year);
  localStorage.setItem(YEAR_STORE_NAME, year);
});

// Lee del localStorage y devuelve el año utilizado la última vez.
function lastUsedYear() {
  return localStorage.getItem(YEAR_STORE_NAME) || currentYear;
}

// Llena el elemento DOM <select> con los valores de los últimos 60 años.
function populateList(selectedYear) {
  [...Array(60)].forEach((_, dec) => {
    const newOption = document.createElement('option');
    const year = currentYear - dec;
    newOption.value = year;
    newOption.innerHTML = year;
    seriesYearSelect.appendChild(newOption);

    // Seleccionamos la opción que corresponde con el valor seleccionado.
    seriesYearSelect.value = selectedYear;
  });
}

// Llena el elemento DOM <ul> con las series publicadas en el año seleccionado.
async function updateSeries(year, offset = 0) {
  // Obtenemos de la API los series de comics
  const series = await api.getSeriesByYear(year.toString(), ITEMS_PAGE, offset);

  seriesCatalog.innerHTML = '';

  // Delegamos al paginador las llamadas consecuentes a la API.
  ui.paginator(ITEMS_PAGE, series.offset, series.total, (offset) => {
    updateContent(lastUsedYear(), offset);
  });

  series.results.forEach((data) => {
    const {
      id,
      title,
      thumbnail,
      type,
      rating,
      comics,
      characters,
      creators,
      startYear,
      endYear,
    } = data;

    // Hacemos uso del script ui para crear un componente record con los
    // valores de la serie en la iteracción actual.
    const serie = ui.record({
      title: ui.removeDates(title),
      image: `${thumbnail.path}/portrait_fantastic.${thumbnail.extension}`,
      url: `series-detail.html?id=${id}`,
      details: {
        duration: ui.formatYears(startYear, endYear),
        type,
        rating,
        comics: comics.items.length,
        characters: characters.items.length,
        creators: creators.items.length,
      },
    });

    const catalogItem = ui.li();
    catalogItem.classList.add('t-tag');

    catalogItem.appendChild(serie);
    seriesCatalog.appendChild(catalogItem);
  });

  if (series.results.length) {
    seriesCatalog.classList.add('l-catalog--is-full');
  }
}
