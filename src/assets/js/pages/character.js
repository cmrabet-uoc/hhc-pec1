/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página character.html.
 * character.js
 * Muestra información sobre el personaje cuyo id se pasa
 * en la query de la URL de la página.
 */

import api from '../lib/api';
import appbar from '../components/appbar';
import ui from '../lib/ui';

// Elemento DOM que contiene la foto del personaje.
const characterPhotoEl = document.querySelector('.js-character-photo');

// Elemento DOM <ul> que contiene la lista de los comics en los que
// aparece el personaje.
const comicsEl = document.querySelector('.js-character-comics');

// Elemento DOM <ul> que contiene la lista de series en las que
// aparece este personaje.
const seriesEl = document.querySelector('.js-character-series');

// Array de elementos DOM que mostrarán el nombre del personaje.
const characterNamesEls = document.querySelectorAll('.js-character-name');

// Elemento DOM que mostrará el número total de series en las que aparece
// el personaje.
const seriesCountEl = document.querySelector('.js-character-series-count');

// Elemento DOM que mostrará el número total de comics en las que aparece
// el personaje.
const comicsCountEl = document.querySelector('.js-character-comics-count');

// Seleccionamos en el menú principal el enlace para series.
appbar.setCurrentPage('characters');

// Populamos el contenido de la página.
updateContent();

async function updateContent() {
  // Extraemos de la URL el id del personaje.
  const urlParams = new URLSearchParams(window.location.search);
  const characterId = urlParams.get('id');

  if (characterId) {
    // Si el id es correcto, obtenemos de la API los datos del personaje.
    const character = await api.getCharacterById(characterId);

    if (character) {
      // Si el personaje existe, poulamos su foto y detalles con los
      // datos devueltos por la API.
      const { name, comics, series, thumbnail } = character;

      characterNamesEls.forEach((item) => (item.innerHTML = name));

      // Le decimos al navegador que cargue diferentes tamaños de imágen en base
      // a su tamaño final en el dispositivo.
      const imageUrl = ui.toHTTPS(thumbnail.path);

      const img = document.createElement('img');
      img.classList.add('comic__cover-image');
      img.src = `${imageUrl}/detail.${thumbnail.extension}`;
      img.srcset = `${imageUrl}/portrait_uncanny.${thumbnail.extension} 300w,`;
      img.srcset += `${imageUrl}/detail.${thumbnail.extension} 500w`;
      img.sizes = '(max-width: 640px) 300px, 500px';

      characterPhotoEl.replaceWith(img);

      // Actualizamos contadores
      seriesCountEl.innerText = series.items.length;
      comicsCountEl.innerText = comics.items.length;

      // Populamos los comics y series en las que aparece el personaje.
      createComicsList(comics.items);
      createSeriesList(series.items);
    }
  }
}

// Popula la lista de comics en las que aparece el personaje.
async function createComicsList(items) {
  comicsEl.innerHTML = '';

  items.forEach(async (item) => {
    // Por cada comic, obtenemos su informacion y creamos un component card usando
    // utilidades del script ui.js.
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getComicById(id);

    const url = `comic.html?id=${id}`;
    const image = `${data.thumbnail.path}/portrait_fantastic.${data.thumbnail.extension}`;
    const card = ui.card({
      title: data.title,
      image,
      url,
    });
    card.classList.add('card--is-comic');

    const comic = ui.li();
    comic.classList.add('catalog__item');
    comic.appendChild(card);

    comicsEl.appendChild(comic);
  });

  if (items.length) {
    comicsEl.classList.add('l-catalog--is-full');
  }
}

// Popula la lista de series en las que aparece el personaje.
async function createSeriesList(items) {
  seriesEl.innerHTML = '';

  items.forEach(async (item) => {
    // Por cada serie, obtenemos su informacion y creamos un elemento <li>
    // con un enlace a la serie.
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getSeriesById(id);

    const url = `series-detail.html?id=${id}`;

    const li = ui.li();
    li.classList.add('l-center');

    const a = ui.a(url, 'link');
    a.classList.add('t-text');
    a.innerText = data.title;
    li.appendChild(a);

    seriesEl.appendChild(li);
  });
}
