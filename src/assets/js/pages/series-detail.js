/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página series-details.html.
 * series-detail.js
 * Muestra información sobre la serie cuyo id se pasa
 * en la query de la URL de la página.
 */

import api from '../lib/api';
import appbar from '../components/appbar';
import ui from '../lib/ui';

// Extrae el id de la serie pasado en la URL cargada y lo almacena.
const urlParams = new URLSearchParams(window.location.search);
const serieId = urlParams.get('id');

// Almacena el elemento DOM que mostrará el nombre de la serie cargada.
const seriesNameEl = document.querySelector('.js-series-name');

// Almacena el elemento DOM que mostrará las fechas en las que se
// publicó la serie cargada.
const seriesDatesEl = document.querySelector('.js-series-dates');

// Almacena el elemento DOM donde se renderizará la lista de descripción
// con los detalles de la serie cargada.
const seriesDetailsEl = document.querySelector('.js-series-details');

// Almacena el elemendo DOM <ul> que contendrá los comics publicados
// en la serie cargada.
const comicsCatalogEl = document.querySelector('.js-comics-catalog');

// Almacena el elemento DOM que contiene el mensaje cuando no hay
// comics en la serie cargada.
// Lo ocultamos por defecto.
const comicsEmptyEl = document.querySelector('.js-comics-empty');

// Seleccionamos en el menú principal el enlace para series.
appbar.setCurrentPage('series');

// Populamos el contenido de la página.
updateContent();

async function updateContent() {
  if (serieId) {
    seriesNameEl.innerText = '...';

    // Obtenemos desde la API los datos de la serie cargada.
    const series = await api.getSeriesById(serieId);
    const {
      title,
      type,
      raiting,
      comics,
      characters,
      creators,
      startYear,
      endYear,
    } = series;

    // Populamos el nombre y años de la serie.
    seriesNameEl.innerHTML = ui.removeDates(title);
    seriesDatesEl.innerHTML = ui.formatYears(startYear, endYear);

    // Creamos la lista de detalles con las estadísticas de la serie cargada.
    const details = ui.dl({
      type,
      raiting,
      comics: comics.available,
      characters: characters.available,
      creators: creators.available,
    });
    details.classList.add('details');

    seriesDetailsEl.innerHTML = '';
    seriesDetailsEl.appendChild(details);

    // Obtenemos desde la API los cómics publicados en la serie cargada.
    const publishedComics = await api.getComicsBySeries(serieId);

    // Si no hay comics, mostramos un mensaje informando al usuario.
    comicsCatalogEl.innerHTML = '';

    // Si no hay cómics, mostramos el texto informando de ello.
    if (!publishedComics.length) {
      comicsEmptyEl.classList.remove('t-hidden');
      return;
    }

    // Por cada cómic creamos un elemento card usando la funcionalidad del
    // script ui.js.
    publishedComics.forEach(({ id, title, thumbnail }) => {
      const comic = ui.card({
        url: `comic.html?id=${id}`,
        title,
        image: `${thumbnail.path}/portrait_fantastic.${thumbnail.extension}`,
      });
      comic.classList.add('card--is-comic');

      const item = ui.li();
      item.classList.add('catalog__item');
      item.appendChild(comic);

      comicsCatalogEl.appendChild(item);
    });

    if (publishedComics.length) {
      comicsCatalogEl.classList.add('l-catalog--is-full');
    }
  }
}
