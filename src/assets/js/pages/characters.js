/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página characters.html.
 * characters.js
 * Muestra una lista con los personajes cuyos nombres
 * empiezan con la letra seleccionada por el usuario.
 */

import api from '../lib/api';
import ui from '../lib/ui';
import appbar from '../components/appbar';

// Número máximo de resultados por página
const ITEMS_PAGE = 20;

// Almacena el nombre de la clave de localStorage para guardar el último
// personaje seleccionado.
// Lo declaro como constante para que sea más cómodo alterar en un futuro.
const LETTER_STORE_NAME = 'marvel-characters-letter';

// Elemento DOM <select> que contendrá las letras del abecedario para
// filtrado de personajes.
const abcListEl = document.querySelector('.js-characters-abc');

// Elemento DOM <ul> que contendrá los personajes filtrados.
const charactersListEl = document.querySelector('.js-characters-list');

// Seleccionamos en el menú principal el enlace para personajes.
appbar.setCurrentPage('characters');

// Llenamos la lista con letras del abecedario.
populateAbc(lastUsedLetter());

// Populamos el contenido de la página.
updateContent(lastUsedLetter());

// Añadimos un listener al evento change y lo respondemos cuando se seleccione
// una nueva letra de abecedario.
abcListEl.addEventListener('change', async function (e) {
  e.preventDefault();
  const selectedLetter = e.target.value || 'A';

  // Actualizamos la list de personajes con la letra seleccionada.
  await updateContent(selectedLetter);

  // Guardamos en localStorage la letra.
  localStorage.setItem(LETTER_STORE_NAME, selectedLetter);
});

// Devuelve la última letra usada (la lee del localStorage).
function lastUsedLetter() {
  return localStorage.getItem(LETTER_STORE_NAME) || 'A';
}

// Llena la lista de letras.
function populateAbc(selectedLetter) {
  [...Array(26)].forEach((_, code) => {
    const letter = String.fromCharCode(code + 65);
    const newOption = document.createElement('option');

    newOption.value = letter;
    newOption.innerHTML = letter;
    abcListEl.appendChild(newOption);
  });

  abcListEl.value = selectedLetter;
}

// Llena la lista de personajes para la letra dada.
async function updateContent(letter, offset = 0) {
  
  // Obtenemos de la API los personajes con nombres que empiezan con la
  // letra pasada como argumento.
  const characters = await api.getCharacters(letter, ITEMS_PAGE, offset);
  
  charactersListEl.innerHTML = '';
  
  // Delegamos al paginador las llamadas consecuentes a la API.
  ui.paginator(ITEMS_PAGE, characters.offset, characters.total, (offset) => {
    updateContent(lastUsedLetter(), offset);
  });

  // Actualizamos el contenido.
  characters.results.forEach(async (item) => {
    const id = ui.getIdFromURI(item.resourceURI);

    // Obtenemos de la API los datos del personaje en la iteracción actual.
    const data = await api.getCharacterById(id);

    const url = `character.html?id=${id}`;
    const image = `${data.thumbnail.path}/portrait_fantastic.${data.thumbnail.extension}`;

    // Cada personaje es un componente card creado con las utilidades
    // del script ui.js.
    const card = ui.card({
      title: data.name,
      image,
      url,
    });
    card.classList.add('card--is-comic');

    const character = ui.li();
    character.classList.add('catalog__item');

    character.appendChild(card);
    charactersListEl.appendChild(character);
  });

  if (characters.results.length) {
    charactersListEl.classList.add('l-catalog--is-full');
  }
}
