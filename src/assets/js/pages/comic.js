/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página comic.html.
 * comic.js
 * Muestra información sobre el comic cuyo id se pasa
 * en la query de la URL de la página.
 */

import api from '../lib/api';
import appbar from '../components/appbar';
import ui from '../lib/ui';

// Elemento DOM que muestra el título del cómic.
const titleEl = document.querySelector('.js-comic-title');

// Elemento DOM que muestra el número de issue del cómic.
const issueEl = document.querySelector('.js-comic-issue');

// Elemento DOM que muestra la imágen de la portada del cómic.
const comicCoverEl = document.querySelector('.js-comic-cover');

// Elemento DOM que contiene la lista de descripción con los
// detalles del cómic.
const detailsEl = document.querySelector('.js-comic-details');

// Elemento DOM que contiene el enlace a la serie a la que pertenece el cómic.
const linkToSeriesEl = document.querySelector('.js-comic-link-to-series');

// Elemento DOM que contiene lista ordenada con los personajes que aparecen
// en este cómic.
const charactersEl = document.querySelector('.js-comic-characters');

// Elemento DOM que contiene lista ordenada con los autores que trabajaron
// en este cómic.
const creatorsEl = document.querySelector('.js-comic-creators');

// Elemento DOM que contiene lista ordenada con otros cómics que aparecen
// en la misma serie del cómic cargado.
const otherComicsEl = document.querySelector('.js-other-comics');

// Elemento DOM que contiene lista ordenada con los personajes que aparecen
// en este cómic.
const seriesNameEl = document.querySelector('.js-comic-series-name');

// Elemento DOM que contiene el mensaje que se muestra cuando no hay
// personajes.
const charactersEmptyEl = document.querySelector('.js-characters-empty');

// Elemento DOM que contiene el mensaje que se muestra cuando no hay
// autores.
const creatorsEmptyEl = document.querySelector('.js-creators-empty');

// Elemento DOM que contiene el mensaje que se muestra cuando no hay
// otros cómics en la misma serie.
const seriesEmptyEl = document.querySelector('.js-series-empty');

// Seleccionamos en el menú principal el enlace para series.
appbar.setCurrentPage('series');

// Populamos el contenido de la página.
updateContent();

/**
 * Helper que busca en un array de objetos una propiedad con un valor
 * determinado.
 * Acepta como argumentos el array (list), el nombre de la propiedad (prop),
 * y el valor que tiene que tener dicha propiedad (value).
 */
function findIn(list, prop, value) {
  return list.find((item) => item[prop] === value);
}

async function updateContent() {
  // Extraemos el id del cómic de la URL del navegador.
  const urlParams = new URLSearchParams(window.location.search);
  const comicId = urlParams.get('id');

  if (comicId) {
    // Obetenemos de la API todos los detalles del cómic.
    const comic = await api.getComicById(comicId);
    if (comic) {
      const {
        title,
        format,
        issueNumber,
        description,
        dates,
        isbn,
        pageCount,
        prices,
        thumbnail,
        characters,
        creators,
        series,
      } = comic;

      // Populamos título, número de issue, y portada.
      titleEl.innerText = ui.removeDates(title);
      issueEl.innerText = `Details for issue ${issueNumber}`;

      // Le decimos al navegador que cargue diferentes tamaños de imágen en base
      // a su tamaño final en el dispositivo.
      const imageUrl = ui.toHTTPS(thumbnail.path);

      const img = document.createElement('img');
      img.classList.add('comic__cover-image');
      img.src = `${imageUrl}/detail.${thumbnail.extension}`;
      img.srcset = `${imageUrl}/portrait_uncanny.${thumbnail.extension} 300w,`;
      img.srcset += `${imageUrl}/detail.${thumbnail.extension} 500w`;
      img.sizes = '(max-width: 640px) 300px, 500px';

      comicCoverEl.replaceWith(img);

      // Extraemos la fecha de venta y precios de la respuesta de la API.
      const onSaleDate = findIn(dates, 'type', 'onsaleDate');
      const printPrice = findIn(prices, 'type', 'printPrice');
      const digitalPrice = findIn(prices, 'type', 'digitalPrice');

      // Populamos la lista de descripción con los detalles del cómic.
      detailsEl.innerHTML = '';
      ui.dl(
        {
          description,
          ['Published On']: onSaleDate && ui.formatTimestamp(onSaleDate.date),
          isbn: isbn ? isbn : '-',
          format: format ? format : '-',
          pages: pageCount > 0 ? pageCount : '-',
          ['Print Price']: printPrice > 0 ? `$${printPrice.price}` : '-',
          ['Digital Price']: digitalPrice ? `$${digitalPrice.price}` : '-',
        },
        true,
        true,
        detailsEl
      );

      // Populamos los personajes que aparecen en el comic.
      createCharactersList(characters.items);

      // Populamos los autores del comic.
      createCreatorsList(creators.items);

      // Creamos una lista con los comics en la misma serie.
      // Primero hay que extraer el id de la serie a la que pertence
      // este comic.
      const seriesId = ui.getIdFromURI(series.resourceURI);
      linkToSeriesEl.href = `/series-detail.html?id=${seriesId}`;

      // Si el id es correcto, obtenemos de la API los detalles de la serie
      // (porque necesitamos su nombre para enseñarlo), y los cómics
      // publicados en ella.
      if (!Number.isNaN(seriesId)) {
        const seriesData = await api.getSeriesById(seriesId);
        const comicsInSeries = await api.getComicsBySeries(seriesId);

        createOtherComics(seriesData.title, comicsInSeries);
      }
    }
  }
}

// Crea la lista de los personajes que aparecen en el comic.
async function createCharactersList(items) {
  charactersEl.innerHTML = '';

  if (!items.length) {
    charactersEl.remove();
    charactersEmptyEl.classList.remove('t-hidden');
    return;
  }

  items.forEach(async (item) => {
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getCharacterById(id);

    const url = `character.html?id=${id}`;
    const image = `${data.thumbnail.path}/portrait_fantastic.${data.thumbnail.extension}`;
    const card = ui.card({
      title: data.name,
      image,
      url,
    });
    card.classList.add('card--is-comic');

    const character = ui.li();
    character.classList.add('catalog__item');

    character.appendChild(card);
    charactersEl.appendChild(character);
  });

  if (items.length) {
    charactersEl.classList.add('l-catalog--is-full');
  }
}

// Crea la lista de los autores del comic.
async function createCreatorsList(items) {
  creatorsEl.innerHTML = '';

  if (!items.length) {
    creatorsEl.remove();
    creatorsEmptyEl.classList.remove('t-hidden');
    return;
  }

  items.forEach(async (item) => {
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getCreatorById(id);

    const url = `creator.html?id=${id}`;

    const a = ui.a(url, 'link');
    a.classList.add('t-text');
    a.innerText = `${data.firstName} ${data.lastName}/${item.role}`;

    const creator = ui.li();
    creator.classList.add('l-center');

    creator.appendChild(a);
    creatorsEl.appendChild(creator);
  });
}

// Crea la lista de otros comics que aparecen en la misma serie
// que el comic cargado.
async function createOtherComics(seriesName, items) {
  seriesNameEl.innerHTML = `${seriesName}`;
  otherComicsEl.innerHTML = '';

  if (!items.length) {
    seriesEmptyEl.classList.remove('t-hidden');
    return;
  }

  items.forEach(async (item) => {
    const id = ui.getIdFromURI(item.resourceURI);
    const data = await api.getComicById(id);

    const url = `comic.html?id=${id}`;
    const image = `${data.thumbnail.path}/portrait_fantastic.${data.thumbnail.extension}`;
    const card = ui.card({
      title: data.title,
      image,
      url,
    });
    card.classList.add('card--is-comic');

    const comic = ui.li();
    comic.classList.add('catalog__item');

    comic.appendChild(card);
    otherComicsEl.appendChild(comic);
  });

  if (items.length) {
    otherComicsEl.classList.add('l-catalog--is-full');
  }
}
