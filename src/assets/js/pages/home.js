/**
 * PEC 1 - Herramientas HTML y CSS 1
 * Chakir Mrabet, Noviembre 2020.
 * Script para página index.html.
 * home.js
 */

import api from '../lib/api';
import ui from '../lib/ui';

// Obtiene de la API los tres últimos comics publicados, y los renderiza en el DOM
// en forma de elementos de una lista no ordenada.

(async function () {
  const homeComicsCatalogEl = document.querySelector('.js-home-comics-catalog');

  const comics = await api.getLastComics();

  homeComicsCatalogEl.innerHTML = '';

  // Por cada comic, renderizamos un componente card utilizando las funciones
  // disponibles en el script ui.js.

  comics.forEach((comic) => {
    const itemEl = ui.li();
    itemEl.classList.add('catalog__item');

    const cardEl = ui.card({
      title: comic.title,
      url: `/comic.html?id=${comic.id}`,
      image: `${comic.thumbnail.path}/portrait_fantastic.${comic.thumbnail.extension}`,
    });
    cardEl.classList.add('card--is-comic');

    itemEl.appendChild(cardEl);
    homeComicsCatalogEl.appendChild(itemEl);
  });

  if (comics.length) {
    homeComicsCatalogEl.classList.add('l-catalog--is-full');
  }
})();
